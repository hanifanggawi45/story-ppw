"""story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from story1 import views as story1_views
from matkul import views as matkul_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('story1/', story1_views.home, name='story1'),
    # path('matkul/',matkul_views.matkul, name='matkul-home'),
    path('matkul/',include('matkul.urls')),
]
