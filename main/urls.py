from django.urls import path
from . import views

# app_name = 'main'

urlpatterns = [
    # path('', views.home, name='home'),
    path('', views.profile, name= 'profile'),
    path('fitur',views.fitur,name='fitur'),
    path('demo/',views.demo, name='demo')
]
