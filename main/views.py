from django.shortcuts import render

import random
# def home(request):
#     return render(request, 'main/home.html')

def profile(request):
    return render(request, "main/profile.html")

def fitur(request):
    return render(request, "main/fitur.html")

def demo(request):
    binatang = ["kucing", "kerbau", "ikan"]
    tempat = ["danau", "padang rumput", "laut"]

    randombinatang = random.choice(binatang)
    randomtempat = random.choice(tempat)

    context = {
        "randomsentence" : randombinatang,
        "randomtempat" : randomtempat
    }


    return render(request, "main/demo.html", context)
