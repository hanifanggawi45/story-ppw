from django.urls import path
from . import views
from .views import (
    MatkulListView, 
    MatkulDetailVIew,
    MatkulCreateView,
    MatkulDeleteView
)

urlpatterns = [
    path('', MatkulListView.as_view(), name= 'matkul-home'),
    path('add', MatkulCreateView.as_view(), name="matkul-add"),
    path('<int:pk>/', MatkulDetailVIew.as_view(), name="matkul-detail"),
    path('matkul/<int:pk>/delete', MatkulDeleteView.as_view(), name="matkul-delete"),
]

#  <app>/<model>_<viewtype>.html