from django.shortcuts import render
from django.views.generic import (
    ListView,
    CreateView,
    DetailView,
    DeleteView
)
from .models import Matkul

# Create your views here.
def matkul(request):
    return render(request, 'matkul/matkul.html')

class MatkulListView(ListView):
    model = Matkul
    template_name = 'matkul/matkul.html'
    content_object_name = 'matkuls'

class MatkulDetailVIew(DetailView):
    model = Matkul

class MatkulCreateView(CreateView):
    model = Matkul
    fields = ["nama", "deskripsi", "dosen", "jumlahSKS", "ruang", "semester"]

class MatkulDeleteView(DeleteView):
    model = Matkul
    success_url = '/matkul'