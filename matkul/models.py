from django.db import models
from django.shortcuts import redirect, reverse
from django.utils.translation import gettext_lazy as _

# Create your models here.
class Matkul(models.Model):
    class Semester(models.TextChoices):
        GEN19 = "Genap 2019/2020", _('Genap 2019/2020')
        GAS19 = "Gasal 2019/2020", _('Gasal 2019/2020')
        GEN20 = "Genap 2020/2021", _('Genap 2020/2021')
        GAS20 = "Gasal 2020/2021", _('Gasal 2020/2021')
        GEN21 = "Genap 2021/2022", _('Genap 2021/2022')
        GAS21 = "Gasal 2021/2022", _('Gasal 2021/2022')
        GEN22 = "Genap 2022/2023", _('Genap 2022/2023')
        GAS22 = "Gasal 2022/2023", _('Gasal 2022/2023')

    nama = models.CharField(max_length=30)
    dosen = models.CharField(max_length=50)
    jumlahSKS = models.IntegerField()
    ruang = models.CharField(max_length=30)
    deskripsi = models.CharField(max_length=200)
    semester = models.CharField(
        max_length=50,
        choices=Semester.choices,
        default=Semester.GEN19
    )

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('matkul-home')


